import { atom } from 'recoil'

export const sidebarOpenState = atom<boolean>({
    key: 'sidebarState',
    default: true,
})
