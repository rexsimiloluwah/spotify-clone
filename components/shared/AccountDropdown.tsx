import React from 'react'
import { ExternalLinkIcon } from '@heroicons/react/outline'
import { signOut } from 'next-auth/react'

interface IAccountDropdownProps {
    open: boolean
}

const AccountDropdown: React.FC<IAccountDropdownProps> = ({ open }) => {
    return (
        <div
            className={`absolute top-10 right-1 min-w-[12rem] rounded-md bg-zinc-900 p-4 shadow-md ${
                open ? '' : 'hidden'
            }`}
        >
            <ul className='flex flex-col gap-3 text-subtle'>
                <div className='flex cursor-pointer items-center justify-between hover:text-white'>
                    <span>Account</span>
                    <ExternalLinkIcon className='h-5 w-5' />
                </div>
                {['Profile', 'Private Session', 'Settings'].map((item, i) => (
                    <li className='cursor-pointer hover:text-white' key={i}>
                        {item}
                    </li>
                ))}
                <div className='cursor-pointer hover:text-white' onClick={() => signOut()}>
                    <hr className='h-1 border-slate-400' />
                    <p className='py-1'>Logout</p>
                </div>
            </ul>
        </div>
    )
}

export default AccountDropdown
