import React from 'react'
import HomeHeader from './HomeHeader'
import { homepageData } from '../data/home'
import HomePlaylistCard from './shared/HomePlaylistCard'
import HomeFooter from './shared/HomeFooter'

const HomeMain: React.FC = () => {
    return (
        <div className='h-screen flex-grow overflow-y-scroll bg-gradient-to-b from-zinc-800 to-black text-white scrollbar-hide'>
            <HomeHeader />
            <div className='px-6 py-4 md:px-10'>
                {homepageData.map((data, id) => (
                    <div key={id} className='my-4 flex flex-col space-y-3'>
                        <div className='flex items-center justify-between'>
                            <h2 className='text-2xl font-semibold md:text-3xl'>{data.title}</h2>
                            <h2 className='cursor-pointer text-[14px] font-semibold text-subtle hover:font-bold hover:underline'>
                                SEE ALL
                            </h2>
                        </div>
                        <div className='grid cursor-pointer gap-4 sm:grid-cols-2 md:grid-cols-5'>
                            {data.content.map((item, id) => (
                                <HomePlaylistCard key={id} {...item} />
                            ))}
                        </div>
                    </div>
                ))}

                <HomeFooter />
            </div>
        </div>
    )
}

export default HomeMain
