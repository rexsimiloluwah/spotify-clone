import React from 'react'
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/outline'
import { useRecoilState } from 'recoil'
import { sidebarOpenState } from '../../atoms/sidebarAtom'

const HeaderControls: React.FC = () => {
    const [sidebarOpen, setSidebarOpen] = useRecoilState(sidebarOpenState)
    return (
        <div className='z-[2000] flex space-x-3'>
            <div
                className={`h-10 w-10 ${
                    sidebarOpen ? 'cursor-pointer' : 'cursor-not-allowed'
                } rounded-full bg-zinc-800 p-1 opacity-80 hover:opacity-100`}
                onClick={() => setSidebarOpen(false)}
            >
                <ChevronLeftIcon className='h-7 w-7' />
            </div>
            <div
                className={`h-10 w-10 ${
                    !sidebarOpen ? 'cursor-pointer' : 'cursor-not-allowed'
                } rounded-full bg-zinc-800 p-1 opacity-80 hover:opacity-100`}
                onClick={() => setSidebarOpen(true)}
            >
                <ChevronRightIcon className='h-7 w-7' />
            </div>
        </div>
    )
}

export default HeaderControls
