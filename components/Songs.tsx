import React from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { playlistState } from '../atoms/playlistAtom'
import Song from './Song'
import { isPlayingState } from '../atoms/songAtom'

const Songs: React.FC = () => {
    // fetch the playlist state
    const playlist = useRecoilValue(playlistState)
    // fetch the song playing state
    const [isPlaying, setIsPlaying] = useRecoilState(isPlayingState)

    // table column headers
    const COLUMN_HEADERS = ['#', 'TITLE', 'ALBUM', 'DATE ADDED', 'TIME']
    return (
        <div className='from-secondary-black bg-gradient-to-t to-zinc-900 px-4 pb-32 text-white md:px-8'>
            <div className='flex cursor-pointer items-center space-x-5 py-3'>
                <div
                    className='rounded-full bg-spotify-green p-2'
                    onClick={() => setIsPlaying(!isPlaying)}
                >
                    {isPlaying ? (
                        <svg role='img' height='32' width='32' viewBox='0 0 24 24'>
                            <path d='M5.7 3a.7.7 0 00-.7.7v16.6a.7.7 0 00.7.7h2.6a.7.7 0 00.7-.7V3.7a.7.7 0 00-.7-.7H5.7zm10 0a.7.7 0 00-.7.7v16.6a.7.7 0 00.7.7h2.6a.7.7 0 00.7-.7V3.7a.7.7 0 00-.7-.7h-2.6z'></path>
                        </svg>
                    ) : (
                        <svg role='img' height='32' width='32' viewBox='0 0 24 24'>
                            <path d='M7.05 3.606l13.49 7.788a.7.7 0 010 1.212L7.05 20.394A.7.7 0 016 19.788V4.212a.7.7 0 011.05-.606z'></path>
                        </svg>
                    )}
                </div>
                <div>
                    <svg
                        role='img'
                        height='32'
                        width='32'
                        viewBox='0 0 24 24'
                        className='fill-[#1ed760]'
                    >
                        <path d='M8.667 1.912a6.257 6.257 0 00-7.462 7.677c.24.906.683 1.747 1.295 2.457l7.955 9.482a2.015 2.015 0 003.09 0l7.956-9.482a6.188 6.188 0 001.382-5.234l-.49.097.49-.099a6.303 6.303 0 00-5.162-4.98h-.002a6.24 6.24 0 00-5.295 1.65.623.623 0 01-.848 0 6.257 6.257 0 00-2.91-1.568z'></path>
                    </svg>
                </div>
                <div>
                    <svg
                        role='img'
                        height='32'
                        width='32'
                        viewBox='0 0 24 24'
                        className='fill-gray-400'
                    >
                        <path d='M4.5 13.5a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm15 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3zm-7.5 0a1.5 1.5 0 100-3 1.5 1.5 0 000 3z'></path>
                    </svg>
                </div>
            </div>
            <div className='overflow-x-auto'>
                <table className='border-spacing w-[100%] table-auto border-collapse'>
                    <thead className='text-sm font-bold md:text-base'>
                        <tr className='p-1 text-left text-subtle'>
                            {COLUMN_HEADERS.map((val, i) => (
                                <th className='py-2 font-normal' key={i}>
                                    {val}
                                </th>
                            ))}
                        </tr>
                    </thead>
                    <tbody>
                        {playlist?.tracks.items.map((item, id) => (
                            <Song
                                key={id}
                                order={id}
                                track={item.track}
                                addedAt={new Date(item.added_at)}
                            />
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default Songs
