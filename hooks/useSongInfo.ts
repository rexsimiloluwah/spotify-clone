import { useEffect, useState } from 'react'
import { useRecoilValue } from 'recoil'
import { currentTrackPlayingIdState } from '../atoms/songAtom'
import useSpotify from './useSpotify'

const useSongInfo = () => {
    const { spotifyApi } = useSpotify()
    const currentTrackPlayingId = useRecoilValue(currentTrackPlayingIdState)
    const [songInfo, setSongInfo] = useState<SpotifyApi.TrackObjectFull | null>(null)
    //console.log('Current track ID here: ', currentTrackPlayingId)
    useEffect(() => {
        const fetchSongInfo = async () => {
            if (currentTrackPlayingId) {
                fetch(`https://api.spotify.com/v1/tracks/${currentTrackPlayingId}`, {
                    headers: {
                        Authorization: `Bearer ${spotifyApi.getAccessToken()}`,
                    },
                })
                    .then((res) => res.json())
                    .then((res) => {
                        console.log('Song Info: ', res)
                        setSongInfo(res)
                    })
                    .catch((error) => {
                        console.error(error)
                    })
            }
        }

        fetchSongInfo()
    }, [currentTrackPlayingId, spotifyApi])

    return songInfo
}

export default useSongInfo
