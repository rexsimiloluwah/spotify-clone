module.exports = {
    content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
    theme: {
        extend: {
            fontSize: {
                sm: ['14px', '20px'],
                base: ['18px', '24px'],
                lg: ['20px', '28px'],
                xl: ['24px', '32px'],
            },
            backgroundColor: {
                'primary-black': '#121212',
                'secondary-black': 'rgb(83, 83, 83)',
                'spotify-green': '#1ed760',
            },
            colors: {
                subtle: '#b3b3b3',
                'spotify-green': '#1ed760',
            },
            fontFamily: {
                sans: 'Cambria, sans-serif',
            },
        },
    },
    plugins: [require('tailwind-scrollbar-hide')],
}
