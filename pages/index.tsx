import React, { useEffect, useState } from 'react'
import type { NextPage } from 'next'
import { getToken } from 'next-auth/jwt'
import Head from 'next/head'
import { Sidebar, UserMain, Player } from '../components'
import { useRecoilValue } from 'recoil'
import { currentTrackPlayingIdState } from '../atoms/songAtom'
import { useSession } from 'next-auth/react'
import useSpotify from '../hooks/useSpotify'
import { useRecoilState, useSetRecoilState } from 'recoil'
import { playlistIdState } from '../atoms/playlistAtom'
import { errorState } from '../atoms/errorsAtom'

const Index: NextPage = () => {
    const { spotifyApi } = useSpotify()
    const { data: session, status } = useSession()
    const [playlists, setPlaylists] = useState<SpotifyApi.ListOfUsersPlaylistsResponse | null>(null)
    const setError = useSetRecoilState(errorState)
    const [playlistId, setPlaylistId] = useRecoilState(playlistIdState)

    useEffect(() => {
        if (spotifyApi.getAccessToken()) {
            spotifyApi
                .getUserPlaylists()
                .then(({ body }) => {
                    setPlaylists(body)
                    // fetch the last playlist id from local storage
                    const defaultPlaylistId = localStorage.getItem('default-playlist-id')
                    setPlaylistId(defaultPlaylistId || body.items[0].id)
                })
                .catch((error) => {
                    console.error(error)
                    setError({
                        status: true,
                        message: 'Could not fetch playlists.',
                    })
                })
        }
    }, [session, spotifyApi, playlistId])

    const currentTrackPlayingId = useRecoilValue(currentTrackPlayingIdState)

    if (status === 'loading') {
        return <h1>Loading...</h1>
    }

    return (
        <div className='h-screen overflow-hidden bg-black'>
            <Head>
                <title>Spotify Clone - Playlists</title>
            </Head>

            <main className='flex'>
                {/* Sidebar */}
                <Sidebar playlists={playlists} />
                {/* Main Section */}
                <UserMain />
            </main>
            {/* Player */}
            <div className='sticky bottom-0'>{currentTrackPlayingId && <Player />}</div>
        </div>
    )
}

export default Index

export const getServerSideProps = async ({ req }) => {
    // Get the token
    // No token is returned if the user is unauthenticated
    const token = await getToken({
        req,
        secret: process.env.JWT_SECRET as string,
    })
    console.log('token: ', token)

    if (!token) {
        return {
            redirect: {
                permanent: false,
                destination: '/home',
            },
            props: {},
        }
    }

    return {
        props: {},
    }
}
