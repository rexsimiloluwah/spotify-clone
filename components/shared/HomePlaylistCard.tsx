import React from 'react'

interface IHomePlaylistCardProps {
    image?: string
    title: string
    description?: string
}

const HomePlaylistCard: React.FC<IHomePlaylistCardProps> = ({ image, title, description }) => {
    return (
        <div className='rounded-md bg-[#181818] p-4 shadow-md ease-in-out hover:-translate-y-2'>
            <img
                src={image}
                alt={title}
                loading='lazy'
                className='w-[100%] rounded-md border-2 border-transparent object-fill'
            />
            <div className='pt-3'>
                <h2 className='text-[18px] font-semibold'>{title}</h2>
                <p className='text-sm text-subtle'>
                    {`${description?.substring(0, 40)}${
                        String(description?.substring(40)).length > 0 ? '...' : ''
                    }`}
                </p>
            </div>
        </div>
    )
}

export default HomePlaylistCard
