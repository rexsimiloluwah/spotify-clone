import { atom } from 'recoil'

export const currentTrackPlayingIdState = atom<string | null>({
    key: 'currentTrackPlayingIdState',
    default: null,
})

export const isPlayingState = atom<boolean>({
    key: 'isPlayingState',
    default: false,
})
