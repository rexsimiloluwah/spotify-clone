import React, { useEffect, useState } from 'react'
import { getProviders, signIn } from 'next-auth/react'
import HeaderControls from './shared/HeaderControls'

const HomeHeader = () => {
    const [authProviders, setAuthProviders] = useState<object | null>(null)
    useEffect(() => {
        const fetchProviders = async () => {
            const providers = await getProviders()
            setAuthProviders(providers)
        }

        fetchProviders()
    }, [])

    const navLinks = [
        {
            title: 'Premium',
            url: 'https://www.spotify.com/ng/premium/?utm_source=app&utm_medium=desktop&utm_campaign=upgrade',
        },
        {
            title: 'Support',
            url: 'https://support.spotify.com/ng/',
        },
        {
            title: 'Download',
            url: 'https://www.spotify.com/ng/download/windows/',
        },
        {
            title: 'Sign Up',
            url: 'https://www.spotify.com/ng/signup',
        },
    ]

    return (
        <header className='flex items-center justify-between bg-zinc-900 py-4 px-10'>
            <HeaderControls />
            <ul className='flex items-center space-x-4'>
                {navLinks.map((link, id) => (
                    <li
                        role={link.title}
                        key={id}
                        className='font-semibold text-subtle hover:text-white'
                    >
                        <a href={link.url} target='_blank' rel='noreferrer'>
                            {link.title}
                        </a>
                    </li>
                ))}
                <li>
                    {authProviders
                        ? Object.values(authProviders).map((provider, id) => (
                              <button
                                  key={id}
                                  onClick={() => signIn(provider.id, { callbackUrl: '/' })}
                                  className='rounded-full bg-white py-[12px] px-8 font-bold text-black hover:scale-105 hover:cursor-pointer hover:bg-gray-100'
                              >
                                  Log In
                              </button>
                          ))
                        : ''}
                </li>
            </ul>
        </header>
    )
}

export default HomeHeader
