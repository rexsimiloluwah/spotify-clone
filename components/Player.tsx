import { useSession } from 'next-auth/react'
import React, { useCallback, useEffect, useState } from 'react'
import { useRecoilState } from 'recoil'
import { currentTrackPlayingIdState, isPlayingState } from '../atoms/songAtom'
import useSongInfo from '../hooks/useSongInfo'
import useSpotify from '../hooks/useSpotify'
import { VolumeUpIcon } from '@heroicons/react/outline'
import {
    FastForwardIcon,
    PauseIcon,
    PlayIcon,
    RewindIcon,
    SwitchHorizontalIcon,
    ReplyIcon,
} from '@heroicons/react/solid'
import { debounce } from 'lodash'
import { spotifyDeviceState } from '../atoms/spotifyDeviceAtom'
import { playlistState } from '../atoms/playlistAtom'
import { useRecoilValue } from 'recoil'

const Player: React.FC = () => {
    const { spotifyApi } = useSpotify()
    const { data: session } = useSession()
    const [currentTrackPlayingId, setCurrentTrackId] = useRecoilState(currentTrackPlayingIdState)
    const [isPlaying, setIsPlaying] = useRecoilState(isPlayingState)
    const [volume, setVolume] = useState<number>(50)
    const [spotifyDevices, setSpotifyDevices] = useRecoilState(spotifyDeviceState)
    // fetch the playlist state
    const playlist = useRecoilValue(playlistState)

    const songInfo = useSongInfo()

    // For fetching the current song
    const fetchCurrentSong = () => {
        if (!songInfo) {
            spotifyApi.getMyCurrentPlayingTrack().then((data) => {
                if (data.body) {
                    // console.log('Now playing: ', data)
                    setCurrentTrackId(data.body.item?.id as string)

                    spotifyApi.getMyCurrentPlaybackState().then((data) => {
                        setIsPlaying(data.body.is_playing)
                    })
                    return
                }
                setCurrentTrackId(null)
                setIsPlaying(false)
            })
        }
    }

    // For handling play and pause of music in the player
    const handlePlayPause = () => {
        spotifyApi
            .getMyCurrentPlaybackState()
            .then((data) => {
                if (data.body.is_playing) {
                    setIsPlaying(false)
                    spotifyApi.pause()
                } else {
                    spotifyApi.play()
                    setIsPlaying(true)
                }
            })
            .catch(() => {
                // console.error(error)
                setSpotifyDevices(null)
            })
    }

    // Play the next song
    const handlePlayNextSong = (forward: number) => {
        // retrieve the playlist's tracks
        const playlistTracks = playlist?.tracks.items as SpotifyApi.PlaylistTrackObject[]
        // find the index of the current playing track in the playlist
        const currentIndex = Number(
            playlistTracks?.findIndex((track) => track.track?.id === currentTrackPlayingId),
        )
        const nextTrack = playlistTracks[currentIndex + forward]
        // play the next track
        spotifyApi
            .play({
                uris: [nextTrack.track?.uri as string],
                device_id: spotifyDevices?.devices[0].id as string,
            })
            .then(() => {
                console.log('Playback started.')
                setCurrentTrackId(nextTrack.track?.id as string)
                setIsPlaying(true)
            })
            .catch((error) => {
                console.error('Something went wrong: ', error)
            })
    }

    // For fetching the current song and setting initial volume
    useEffect(() => {
        if (spotifyApi.getAccessToken() && !currentTrackPlayingId) {
            fetchCurrentSong()
            setVolume(50)
        }
    }, [currentTrackPlayingId, spotifyApi, session])

    useEffect(() => {
        if (volume > 0 && volume < 100) {
            debounceAdjustVolume(volume)
        }
    }, [volume])

    // Debouncing the volume adjust button
    const debounceAdjustVolume = useCallback(
        debounce((volume) => {
            spotifyApi.setVolume(volume).catch((error) => console.error(error))
        }, 500),
        [],
    )
    return (
        <div>
            <div className='grid h-20 grid-cols-3 border-t border-gray-500 bg-primary-black px-2 text-xs text-white md:px-8 md:text-base'>
                {/* Left part */}
                <div className='flex items-center space-x-2'>
                    <img
                        className='hidden h-10 w-10 md:inline'
                        src={songInfo?.album.images[0].url}
                        alt=''
                    />
                    <div>
                        <h3 className='text-semibold'>{songInfo?.name}</h3>
                        <p className='text-subtle'>
                            {songInfo?.artists.map((artist) => artist.name).join(', ')}
                        </p>
                    </div>
                </div>

                {/* Center Part */}
                <div className='flex items-center justify-evenly'>
                    <SwitchHorizontalIcon className='player-button' />
                    <button onClick={() => handlePlayNextSong(-1)}>
                        <RewindIcon className='player-button' />
                    </button>
                    <button type='button' onClick={handlePlayPause}>
                        {isPlaying ? (
                            <PauseIcon className='player-button h-14 w-14' />
                        ) : (
                            <PlayIcon className='player-button h-14 w-14' />
                        )}
                    </button>
                    <button onClick={() => handlePlayNextSong(1)}>
                        <FastForwardIcon className='player-button' />
                    </button>
                    <ReplyIcon className='player-button' />
                </div>

                {/* Right part */}
                <div className='flex items-center justify-end space-x-3'>
                    <input
                        type='range'
                        value={volume}
                        min={0}
                        max={100}
                        className='w-14 cursor-pointer md:w-28'
                        onChange={(e) => setVolume(Number(e.target.value))}
                    />
                    <VolumeUpIcon
                        onClick={() => setVolume(volume + 10)}
                        className='text-light player-button h-6 w-7'
                    />
                </div>
            </div>
            {spotifyDevices && spotifyDevices.devices.length ? (
                <div className='flex items-center justify-end space-x-1 bg-spotify-green px-8'>
                    <VolumeUpIcon className='h-5 w-5' />
                    <span>Listening on {spotifyDevices?.devices[0].name}</span>
                </div>
            ) : (
                <div className='bg-red-500 px-8 text-right text-white'>No spotify device found</div>
            )}
        </div>
    )
}

export default Player
