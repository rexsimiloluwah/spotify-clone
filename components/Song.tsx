import React from 'react'
import { useRecoilState, useSetRecoilState } from 'recoil'
import { currentTrackPlayingIdState, isPlayingState } from '../atoms/songAtom'
import useSpotify from '../hooks/useSpotify'
import { spotifyDeviceState } from '../atoms/spotifyDeviceAtom'
import { Audio } from 'react-loader-spinner'

interface SongProps {
    order: number
    track: SpotifyApi.TrackObjectFull | null
    addedAt: Date
}

const Song: React.FC<SongProps> = ({ order, track, addedAt }) => {
    const { spotifyApi, getSpotifyDevices } = useSpotify()
    // for fetching the current playing track ID
    const [currentTrackId, setCurrentTrackId] = useRecoilState(currentTrackPlayingIdState)
    // for fetching and setting the playing state
    const [isPlaying, setIsPlaying] = useRecoilState(isPlayingState)
    // for setting the spotify devices state
    const setSpotifyDevices = useSetRecoilState(spotifyDeviceState)

    // Play a song, when clicked
    const handlePlaySong = async () => {
        // set the current track as the clicked track
        setCurrentTrackId(track?.id as string)
        // fetch the active spotify devices from the Spotify API
        getSpotifyDevices()
            .then(({ body }) => {
                // console.log('Getting spotify devices')
                // set the spotify devices state
                setSpotifyDevices(body)
                if (body.devices.length) {
                    // play the song is there is an active spotify devices
                    spotifyApi
                        .play({
                            uris: [track?.uri as string],
                            device_id: body.devices[0].id as string,
                        })
                        .then(() => {
                            console.log('Playback started.')
                            setIsPlaying(true)
                        })
                        .catch((error) => {
                            console.error('Something went wrong: ', error)
                        })
                }
            })
            .catch((error) => {
                console.error(error)
            })
    }

    // Formatting time/date when the song was added to the playlist
    const formatTimeDifference = (addedAt: Date): string => {
        const currentTime = Date.now()
        const timeDifference = (currentTime - addedAt.getTime()) / (1000 * 60 * 60)
        if (timeDifference < 24) {
            return `${Math.round(timeDifference)} hours ago`
        }
        return addedAt.toLocaleDateString()
    }

    // Utility function for formatting the song duration --> mm:ss format
    const formatSongDuration = (durationMs: number): string => {
        // convert duration to seconds
        let duration = durationMs / 1000
        let mins = 0
        while (duration >= 60) {
            mins += 1
            duration = duration - 60
        }
        //format
        return [mins, Math.round(duration).toString().padStart(2, '0')].join(':')
    }

    return (
        <tr
            className='group rounded-md text-sm hover:cursor-pointer hover:bg-slate-900 hover:text-white md:text-base '
            onClick={handlePlaySong}
        >
            <td className='w-5 rounded-l-md py-2 pr-2 group-hover:text-white'>
                {isPlaying && track?.id === currentTrackId ? (
                    <Audio height='18' width='18' color='#1ed760' />
                ) : (
                    order + 1
                )}
            </td>
            <td className='flex items-center space-x-2 py-2 group-hover:text-white'>
                <img src={track?.album.images[0].url} alt='' className='h-10 w-10' />
                <div>
                    <h1
                        className={`${
                            isPlaying && currentTrackId === track?.id ? 'text-spotify-green' : ''
                        }`}
                    >
                        {track?.name}
                    </h1>
                    <p className='text-[14px] font-semibold text-subtle group-hover:text-white'>
                        {track?.artists.map((artist) => artist.name).join(' , ')}
                    </p>
                </div>
            </td>
            <td className='text-subtle group-hover:text-white'>{track?.album.name}</td>
            <td className='w-[15%] text-subtle group-hover:text-white'>
                {formatTimeDifference(addedAt)}
            </td>
            <td className='rounded-r-md text-subtle group-hover:text-white'>
                {formatSongDuration(track?.duration_ms as number)}
            </td>
        </tr>
    )
}

export default Song
