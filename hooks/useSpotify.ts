import { signIn, useSession } from 'next-auth/react'
import { useEffect } from 'react'
import spotifyApi from '../lib/spotify'

const useSpotify = () => {
    const { data: session } = useSession()
    useEffect(() => {
        //console.log('session: ', session)
        if (session) {
            // if refresh access token fails, redirect them to the sign in page
            if (session.error === 'RefreshAccessTokenError') {
                signIn()
            }

            // @ts-ignore
            spotifyApi.setAccessToken(session.accessToken)
            // @ts-ignore
            spotifyApi.setRefreshToken(session.refreshToken)
        }
    }, [session])

    const getSpotifyDevices = async () => {
        return spotifyApi.getMyDevices()
    }

    return { spotifyApi, getSpotifyDevices }
}

export default useSpotify
