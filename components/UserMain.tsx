import React, { useEffect, useState } from 'react'
import { ExclamationCircleIcon, MusicNoteIcon } from '@heroicons/react/outline'
import { useSession } from 'next-auth/react'
import useSpotify from '../hooks/useSpotify'
import { useRecoilState, useRecoilValue } from 'recoil'
import { playlistIdState, playlistState } from '../atoms/playlistAtom'
import Songs from './Songs'
import { errorState } from '../atoms/errorsAtom'
import { ThreeDots } from 'react-loader-spinner'
import { loadingState } from '../atoms/loadingAtom'
import Header from './UserHeader'

const UserMain: React.FC = () => {
    const { data: session, status } = useSession()
    const { spotifyApi } = useSpotify()
    const [playlist, setPlaylist] = useRecoilState(playlistState)
    const [error, setError] = useRecoilState(errorState)
    const playlistId = useRecoilValue(playlistIdState)
    const [headerColor, setHeaderColor] = useState('')
    const [loading, setLoading] = useRecoilState(loadingState)

    // Set a random header color
    useEffect(() => {
        setHeaderColor(HEADER_COLORS[Math.floor(Math.random() * HEADER_COLORS.length)])
    }, [playlistId])

    // Fetch a user's playlist tracks
    useEffect(() => {
        if (spotifyApi.getAccessToken()) {
            // only refresh if there is no playlist available
            if (!playlist) {
                setLoading(true)
            }
            if (playlistId) {
                spotifyApi
                    .getPlaylist(playlistId)
                    .then((data) => {
                        setLoading(false)
                        // console.log('current playlist:', data)
                        setPlaylist(data.body)
                        setError(null)
                    })
                    .catch(() => {
                        setLoading(false)
                        // console.error('An error occurred: ', error)
                        setError({
                            status: true,
                            message: `Could not fetch playlist tracks. You are possibly offline.`,
                        })
                    })
            }
        }
    }, [playlistId, session])

    const HEADER_COLORS = [
        'from-indigo-500',
        'from-blue-500',
        'from-green-500',
        'from-red-500',
        'from-yellow-500',
        'from-purple-500',
    ]

    // Render a simple loading indicator when the user session is in the loading state
    if (status === 'loading') {
        return <h1>Loading...</h1>
    }

    // Render an error message when an error occurs while loading the
    // playlists from the Spotify API
    if (error?.status) {
        return (
            <div className='m-auto flex h-screen flex-grow flex-col items-center justify-center gap-4 bg-primary-black text-white'>
                <ExclamationCircleIcon className='h-24 w-24' />
                <h1 className='text-center text-3xl font-bold'>An Error Occurred</h1>
                <p>{error.message}</p>
            </div>
        )
    }

    // Render a loading spinner when the playlists are loading
    if (loading) {
        return (
            <div className='flex h-screen flex-grow items-center justify-center'>
                <ThreeDots height='100' width='100' color='grey' ariaLabel='loading' />
            </div>
        )
    }

    return (
        <div className='relative h-screen flex-grow overflow-y-scroll bg-primary-black text-white scrollbar-hide'>
            <Header user={session?.user} />
            <section
                className={`flex h-80 items-end space-x-3 bg-gradient-to-b md:space-x-7 ${headerColor} to-black py-8 px-4 text-white md:px-8`}
            >
                {playlist?.images[0] ? (
                    <img
                        src={playlist?.images[0].url || '/images/spotify-logo.png'}
                        alt=''
                        className='h-36 w-32 shadow-2xl md:h-44 md:w-44'
                    />
                ) : (
                    <div className='text-light rounded-lg bg-[#282828] p-5 text-center shadow-lg'>
                        <MusicNoteIcon className='h-32 w-32' />
                    </div>
                )}
                <div className='space-y-3'>
                    <p className='font-semibold'>
                        {playlist?.public ? 'PUBLIC' : 'PRIVATE'} PLAYLIST
                    </p>
                    <h1 className='text-2xl font-bold md:text-6xl xl:text-7xl'>{playlist?.name}</h1>
                    <p className='font-semibold text-gray-500'>{playlist?.description}</p>
                    <div className='flex items-center space-x-2'>
                        <div className='flex items-center space-x-1'>
                            <img src='/images/spotify-logo.png' alt='' className='h-5 w-5' />{' '}
                            <span className='font-semibold'>Spotify</span>
                        </div>
                        <span className='inline-block h-1 w-1 rounded-full bg-white'></span>
                        <p>{playlist?.followers.total} likes</p>
                        <span className='inline-block h-1 w-1 rounded-full bg-white'></span>
                        <p>{playlist?.tracks.total} songs</p>
                    </div>
                </div>
            </section>

            <Songs />
        </div>
    )
}

export default UserMain
