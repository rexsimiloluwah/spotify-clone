import { atom } from 'recoil'

interface IError {
    status: boolean
    message?: string | null
}
export const errorState = atom<IError | null>({
    key: 'errorState',
    default: null,
})
