import { atom } from 'recoil'

export const screenSizeState = atom<number | null>({
    key: 'screenSizeState',
    default: null,
})
