import React from 'react'
import type { InferGetServerSidePropsType } from 'next'
import { getProviders } from 'next-auth/react'
import Head from 'next/head'
import { Sidebar, HomeMain } from '../components'

const Login = ({ providers }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
    console.log(providers)
    return (
        <div className='h-screen overflow-hidden bg-black'>
            <Head>
                <title>Spotify Clone - Playlists</title>
            </Head>

            <main className='flex'>
                {/* Sidebar */}
                <Sidebar />
                {/* Main Section */}
                <HomeMain />
            </main>
        </div>
    )
}

export default Login

export async function getServerSideProps() {
    const providers = await getProviders()

    return {
        props: {
            providers,
        },
    }
}
