import { atom } from 'recoil'

export const spotifyDeviceState = atom<SpotifyApi.UserDevicesResponse | null>({
    key: 'spotifyDeviceState',
    default: null,
})
